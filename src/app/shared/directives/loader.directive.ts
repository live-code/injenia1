import { ComponentFactoryResolver, ComponentRef, Directive, EventEmitter, Input, Output, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../components/card.component';
import { COMPONENTS } from '../../app.component';

@Directive({
  selector: '[appLoader]',
})
export class LoaderDirective {
  @Output() action: EventEmitter<any> = new EventEmitter<any>();

  @Input() set appLoader(config) {
    const factory = this.resolver.resolveComponentFactory(COMPONENTS[config.type]);
    const compo: ComponentRef<any> = this.view.createComponent(factory);

   //  setTimeout(() => {
      for (const key in config.payload) {
        compo.instance[key] = config.payload[key];
      }

      if (config.events) {
        compo.instance[config.events.type].subscribe(
          () => this.action.emit(config.events.command)
        );
      }

      try {
        compo.instance.update();
      } catch (e) {}
    // }, 3000);

  }
  constructor(
    private view: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) { }

}
