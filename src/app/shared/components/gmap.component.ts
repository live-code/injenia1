import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'app-gmap',
  template: `
    <div 
      #host
      style="width: 100%"
      [style.height.px]="height"
    ></div>
  `,
  styles: [
  ]
})
export class GmapComponent implements OnInit, OnChanges {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLDivElement>;
  @Input() lat: number;
  @Input() lng: number;
  @Input() zoom = 5;
  @Input() height = 300;
  @Output() setZoom: EventEmitter<number> = new EventEmitter<number>();
  @Output() markerClick: EventEmitter<void> = new EventEmitter<void>();
  map: google.maps.Map;

  constructor(
    private cd: ChangeDetectorRef
  ) {

  }

  ngOnInit() {
    console.log('init', this.lat)
    if (!this.map) {
      this.initMap();
    }

    this.render(
      this.lat,
      this.lng,
      this.zoom
    );
  }

  initMap() {
    this.map = new google.maps.Map<Element>(
      this.host.nativeElement,
      { zoom: this.zoom}
    );

    google.maps.event.addListener(this.map, 'zoom_changed', () => {
      this.setZoom.emit(this.map.getZoom())
    });

    google.maps.event.addListener(this.map, 'zoom_changed', () => {
      this.setZoom.emit(this.map.getZoom())
    });
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (!this.map) {
      this.initMap();
    }

    this.render(
      changes.lat?.currentValue,
      changes.lng?.currentValue,
      changes.zoom?.currentValue
    );

  }

  render(lat: number, lng: number, zoom: number) {
    if (lat && lng) {
      const position = { lat, lng };
      this.map.setCenter(position);
      // to improve
      /*
      const marker = new google.maps.Marker({position, map: this.map});
      marker.addListener('click', (ev) => {
        this.markerClick.emit()
      });
      */

    }

    if (zoom) {
      this.map.setZoom(zoom);
    }
  }

  update() {
    console.log(this.lat);
    this.ngOnInit();
  }
}
