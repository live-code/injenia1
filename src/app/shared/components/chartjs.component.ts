import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Chart, ChartConfiguration, ChartType } from 'chart.js';

@Component({
  selector: 'app-chartjs',
  template: `
    <div style="width: 100%">
      <canvas #host width="400" height="400"></canvas>
    </div>
  `,
  styles: [
  ]
})
export class ChartjsComponent implements OnChanges {
  @ViewChild('host', { static: true}) host: ElementRef<HTMLCanvasElement>;
  @Input() config: ChartConfiguration;
  @Input() type: ChartType = 'line';
  myChart: Chart;

  ngOnChanges(changes: SimpleChanges ) {
    if (changes.config.isFirstChange()) {
      this.myChart = new Chart(
        this.host.nativeElement.getContext('2d'),
        {
          ...changes.config.currentValue,
          type: this.type
        }
      );
    } else {
      this.myChart.config = {
        ...changes.config.currentValue,
        type: this.type
      };
      this.myChart.update();
    }
  }
}

