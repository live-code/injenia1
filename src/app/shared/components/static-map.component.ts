import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-map',
  template: `
    <img
      *ngIf="value"
      [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + value + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
  `,
  styles: [
  ]
})
export class StaticMapComponent {
  @Input() value: string;
}
