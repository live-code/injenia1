import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        *ngFor="let item of items"
        class="nav-item"
        (click)="tabClick.emit(item)"
      >
        <a 
          class="nav-link" 
          [ngClass]="{'active': item.id === active?.id}"
        >
          {{item[labelField]}}
          
          <i [class]="icon" (click)="iconClick.emit(item)"></i>
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent <T> {
  @Input() items: T[];
  @Input() active: T;
  @Input() icon: string;
  @Input() labelField = 'label';
  @Output() tabClick: EventEmitter<T> = new EventEmitter();
  @Output() iconClick: EventEmitter<T> = new EventEmitter();
}
