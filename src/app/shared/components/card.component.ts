import { Component, ContentChild, ElementRef, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div 
      class="card" 
      [ngClass]="'mb-' + marginBottom"
    >
      <div
        class="card-header"
        *ngIf="header"
        [style.backgroundColor]="headerBgColor"
        [ngClass]="{
          'bg-dark text-white': theme === 'dark', 
          'bg-light': theme === 'light' 
        }"
        (click)="isOpen = !isOpen"
      >
        {{header}}

        <div class="pull-right" *ngIf="icon">
          <i 
            [class]="'fa fa-' + icon" 
            (click)="iconClick.emit()"
          ></i>
        </div>
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content #content></ng-content>
        <hr>
        <ng-content select=".footer"></ng-content>
      </div>
    </div>
  `,
  providers: [
  ]
})
export class CardComponent {
  @Input() header: string;
  @Input() icon: string;
  @Input() marginBottom = 3;
  @Input() headerBgColor: string;
  @Input() theme: 'dark' | 'light' = 'dark';
  @Output() iconClick: EventEmitter<void> = new EventEmitter();
  @ContentChild('content') content: ElementRef;

  isOpen = true;
}
