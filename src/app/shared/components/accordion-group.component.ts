import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-accordion-group',
  template: `
    <div class="card">
      <div class="card-header" (click)="toggle.emit()">
        {{title}}
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AccordionGroupComponent  {
  @Input() title: string;
  @Input() isOpen = false;
  @Output() toggle: EventEmitter<void> = new EventEmitter<void>();
}
