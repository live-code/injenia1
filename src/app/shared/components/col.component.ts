import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-col',
  template: `
    <ng-content></ng-content>
  `,
})
export class ColComponent  {
  @HostBinding() className = 'col';
  // @HostBinding('className') cls  = 'col';
  // @HostBinding('style.color') color = 'red';
}
