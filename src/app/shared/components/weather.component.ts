import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Meteo } from '../model/meteo';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

const API = 'http://api.openweathermap.org/data/2.5/weather';
const TOKEN = 'eb03b1f5e5afb5f4a4edb40c1ef2f534';

@Component({
  selector: 'app-weather',
  template: `
    <div *ngIf="meteo">
      {{meteo.name}}
      {{meteo.main?.temp}}°
      <img
        [src]="'https://openweathermap.org/img/w/' + meteo?.weather[0].icon + '.png'" alt="">
    </div>
  `,
})
export class WeatherComponent implements OnChanges {
  meteo: Meteo;
  @Input() city: string;
  @Input() unit: 'metric' | 'imperial' = 'metric';

  /*
  @Input() set city( city: string) {
    if (city) {
      this.http.get<Meteo>(`${API}?q=${city}&units=metric&APPID=${TOKEN}`)
        .subscribe(res => this.meteo = res)
    }
  }
  */

  constructor(private http: HttpClient) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.city?.currentValue) {
      this.http.get<Meteo>(`${API}?q=${this.city}&units=${this.unit}&APPID=${TOKEN}`)
        .pipe(
          catchError(() => of(null))
        )
        .subscribe(res => this.meteo = res);
    }
  }
}
