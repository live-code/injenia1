import { CardComponent } from './card.component';
import { TabbarComponent } from './tabbar.component';
import { StaticMapComponent } from './static-map.component';
import { RowComponent } from './row.component';
import { ColComponent } from './col.component';
import { AccordionComponent } from './accordion.component';
import { AccordionGroupComponent } from './accordion-group.component';
import { WeatherComponent } from './weather.component';
import { ChartjsComponent } from './chartjs.component';
import { GmapComponent } from './gmap.component';
import { LoaderDirective } from '../directives/loader.directive';

export const COMPONENTS = [
  CardComponent,
  TabbarComponent,
  StaticMapComponent,
  RowComponent,
  ColComponent,
  AccordionComponent,
  AccordionGroupComponent,
  WeatherComponent,
  ChartjsComponent,
  GmapComponent,
];
