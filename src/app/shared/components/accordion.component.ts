import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChildren,
  Input,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { AccordionGroupComponent } from './accordion-group.component';

@Component({
  selector: 'app-accordion',
  template: `
    <div class="accordion">
      <ng-content></ng-content>
    </div>
  `,
  styles: [`
    .accordion {
      background-color: white;
      border: 1px solid #222;
      color: #222;
    }
  `]
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(AccordionGroupComponent) groups: QueryList<AccordionGroupComponent>;
  @Input() openOnce: boolean;

  ngAfterContentInit() {
    this.groups.toArray()[0].isOpen = true;
    this.groups.toArray().forEach(g => {
      g.toggle.subscribe(() => this.openGroup(g));
    });
  }

  openGroup(activeGroup: AccordionGroupComponent) {
    if (this.openOnce) {
      this.groups.toArray().forEach(g => {
        g.isOpen = false;
      });
    }
    activeGroup.isOpen = !activeGroup.isOpen;
  }

}
