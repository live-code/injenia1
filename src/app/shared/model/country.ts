export class Country {
  id: number;
  label: string;
  temp: Array<number>;
  cities: City[];
}

export interface City {
  id: number;
  name: string;
}
