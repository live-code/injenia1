import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `
    <button routerLink="/" class="btn" [routerLinkActiveOptions]="{ exact: true}" routerLinkActive="btn-primary" >home</button>
    <button routerLink="uikit" class="btn" routerLinkActive="btn-primary" >uikit</button>
    <button routerLink="catalog" class="btn" routerLinkActive="btn-primary" >catalog</button>
    <button [routerLink]="'customers'" class="btn" routerLinkActive="btn-primary" >customers</button>
    <hr>

  `,
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
