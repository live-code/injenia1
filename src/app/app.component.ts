import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { ChartConfiguration } from 'chart.js';
import { CardComponent } from './shared/components/card.component';
import { TabbarComponent } from './shared/components/tabbar.component';
import { GmapComponent } from './shared/components/gmap.component';


export const COMPONENTS = {
  ['card']: CardComponent,
  ['tabbar']: TabbarComponent,
  ['gmap']: GmapComponent,
}

@Component(/**/{
  selector: 'app-root',
  template: `
    
    <app-navbar></app-navbar>
    
    <div class="container mt-3">
      <router-outlet></router-outlet>
      <!--<app-uikit-card></app-uikit-card>-->
      <!--<app-uikit-tabbar></app-uikit-tabbar>-->
      <!--<app-uikit-grid></app-uikit-grid>-->
      <!--<app-uikit-accordion></app-uikit-accordion>-->
      <!--<app-uikit-weather></app-uikit-weather>-->
      <!--<app-uikit-charts></app-uikit-charts>-->
      <!--<app-uikit-maps></app-uikit-maps>-->
      <!--<app-uikit-loader></app-uikit-loader>-->
      
    </div>
  `,
})
export class AppComponent {

}




