import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uikit-grid',
  template: `
    <div class="row">
      <div class="col">1</div>
      <div class="col">2</div>
      <div class="col">3</div>
    </div>

    <app-row>
      <app-col>1</app-col>
      <app-col>
        <button></button>
      </app-col>
      <app-col>3</app-col>
    </app-row>
  `,
  styles: [
  ]
})
export class UikitGridComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
