import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uikit-card',
  template: `
    <div class="container mt-3">
      <app-card header="title only - no body"></app-card>
      
      <app-card
        header="Card with title and body"
        icon="link"
        headerBgColor="lightgreen"
        [marginBottom]="5"
        [theme]="'light'"
        (iconClick)="goto('http://www.google.com')"
      >
        <button>CLICK ME</button>
      </app-card>

      <app-card header="xyz" icon="trash" (iconClick)="doSomething()">
        <input type="text"> <br>
        <input type="text"> <br>
        <input type="text"> <br>

        <div class="footer">
          Sono un footer
        </div>
      </app-card>


      <app-card header="ROOT">
        <div class="row">
          <div class="col">
            <app-card header="1">
              desc 1
            </app-card>
          </div>
          <div class="col">
            <app-card header="1">
              desc 1
            </app-card>
          </div>
        </div>

      </app-card>
    </div>
  `,
  styles: [
  ]
})
export class UikitCardComponent {

  goto(url: string) {
    window.open(url);
  }

  doSomething() {
    console.log('do something');
  }

}
