import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uikit-loader',
  template: `

    <ng-container
      *ngFor="let c of config"
      [appLoader]="c"
      (action)="runCommand($event)"
    ></ng-container>

  `,
  styles: [
  ]
})
export class UikitLoaderComponent {

  config = [
    {
      type: 'card',
      payload: { header: 'one', icon: 'link' },
      events: {
        type: 'iconClick',
        command: 'xyz'
      }
    },
    {
      type: 'tabbar',
      payload: {
        items: [
          { id: 1, label: 'one'},
          { id: 2, label: 'two'},
        ],
        active: { id: 2, label: 'two'},
      },

    },
    {
      type: 'gmap',
      payload: {
        lat: 53,
        lng: 20
      },

    },
  ]

  runCommand(key: string) {
    console.log(key);
  }
}
