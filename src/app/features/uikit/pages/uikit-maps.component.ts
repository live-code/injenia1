import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uikit-maps',
  template: `
    <app-gmap 
      [lat]="country.lat" 
      [lng]="country.lng"
      [zoom]="zoom"
      (setZoom)="zoom = $event"
      (markerClick)="markerClickHandler()"
    ></app-gmap>
    zoom: {{zoom}}
    
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="zoom = zoom - 1">-</button>
  `,
  styles: [
  ]
})
export class UikitMapsComponent implements OnInit {
  country = { lat: 43, lng: 13}
  zoom = 5;

  constructor() {
    setTimeout(() => {
      this.country = { lat: 41, lng: 12}
    }, 2000)
  }

  ngOnInit(): void {
  }

  markerClickHandler() {
    console.log('click')
  }
}
