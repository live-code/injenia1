import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { catchError, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../../../shared/model/meteo';
import { of } from 'rxjs';

@Component({
  selector: 'app-uikit-weather',
  template: `
    <input type="text" [formControl]="input" placeholder="write a city">
    <app-weather unit="metric" [city]="city"></app-weather>
  `,
})
export class UikitWeatherComponent implements OnInit {
  input: FormControl = new FormControl('');
  city: string;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.input.valueChanges
      .pipe(
        debounceTime(1000),
        distinctUntilChanged(),
      )
      .subscribe(text => this.city = text);
  }

}
