import { Component } from '@angular/core';

@Component({
  selector: 'app-uikit-accordion',
  template: `
    <app-accordion [openOnce]="false">
      <app-accordion-group title="1">
        bla bla
      </app-accordion-group>
      <app-accordion-group title="2">
        bla bla 2
      </app-accordion-group>
      <app-accordion-group title="3">
        bla bla 3
      </app-accordion-group>
    </app-accordion>
  `,
})
export class UikitAccordionComponent { }
