import { Component, OnInit } from '@angular/core';
import { City, Country } from '../../../shared/model/country';

@Component({
  selector: 'app-uikit-tabbar',
  template: `
    <app-tabbar
      [items]="countries"
      [active]="selectedCountry"
      icon="fa fa-wikipedia-w"
      (tabClick)="setCountry($event)"
      (iconClick)="gotoWikipedia($event)"
    ></app-tabbar>

    <app-tabbar
      [items]="selectedCountry.cities"
      [active]="selectedCity"
      labelField="name"
      icon="fa fa-trash"
      (tabClick)="setCity($event)"
      (iconClick)="deleteCity($event)"
    ></app-tabbar>
    
    <app-static-map [value]="selectedCity?.name"></app-static-map>
  `,
})
export class UikitTabbarComponent {
  countries: Country[] = [
    {
      id: 1001, label: 'Japan', temp: [12, 19, 3, 5, 2, 3],
      cities: [
        { id: 1, name: 'Tokyo'},
        { id: 2, name: 'Osaka'},
      ]
    },
    {
      id: 1002, label: 'Italy', temp: [12, 19, 3, 35, 2, 3],
      cities: [
        { id: 11, name: 'Rome'},
        { id: 21, name: 'Trieste'},
        { id: 31, name: 'Bologna'},
      ]
    },
    {
      id: 1003, label: 'USA', temp: [12, 19, 23, 25, 12, 3],
      cities: [
        { id: 133, name: 'New York'},
      ]
    }
  ];
  selectedCountry: Country;
  selectedCity: City;

  constructor() {
    this.setCountry(this.countries[0]);
  }

  setCountry(c: Country) {
    this.selectedCountry = c;
    this.selectedCity = c.cities[0];
  }
  setCity(c: City) {
    this.selectedCity = c;
  }

  gotoWikipedia(c: Country) {
    window.open('https://it.wikipedia.org/wiki/' + c.label)
  }

  deleteCity(city: City) {
    const index = this.selectedCountry.cities.findIndex(c => c.id === city.id);
    this.selectedCountry.cities.splice(index, 1);
  }
}
