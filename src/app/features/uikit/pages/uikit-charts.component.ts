import { Component, OnInit } from '@angular/core';
import { ChartConfiguration } from 'chart.js';
import { CHART_CONFIGURATION, updateDataSet } from '../../../shared/components/charts.utils';

@Component({
  selector: 'app-uikit-charts',
  template: `
    <app-chartjs [config]="configuration"></app-chartjs>
    <app-chartjs [config]="configuration" type="pie"></app-chartjs>
  `,
})
export class UikitChartsComponent implements OnInit {
  configuration: ChartConfiguration = CHART_CONFIGURATION;

  ngOnInit(): void {
    setTimeout(() => {
      this.configuration = updateDataSet(
        [10, 20, 30, 40, 50, 60]
      );
    }, 1000);

  }

}
