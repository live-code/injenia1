import { NgModule } from '@angular/core';
import { UikitCardComponent } from './pages/uikit-card.component';
import { UikitTabbarComponent } from './pages/uikit-tabbar.component';
import { UikitGridComponent } from './pages/uikit-grid.component';
import { UikitAccordionComponent } from './pages/uikit-accordion.component';
import { UikitWeatherComponent } from './pages/uikit-weather.component';
import { UikitChartsComponent } from './pages/uikit-charts.component';
import { UikitMapsComponent } from './pages/uikit-maps.component';
import { UikitLoaderComponent } from './pages/uikit-loader.component';
import { UikitComponent } from './uikit.component';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../../shared/components/card.component';
import { TabbarComponent } from '../../shared/components/tabbar.component';
import { StaticMapComponent } from '../../shared/components/static-map.component';
import { RowComponent } from '../../shared/components/row.component';
import { ColComponent } from '../../shared/components/col.component';
import { AccordionComponent } from '../../shared/components/accordion.component';
import { AccordionGroupComponent } from '../../shared/components/accordion-group.component';
import { WeatherComponent } from '../../shared/components/weather.component';
import { ChartjsComponent } from '../../shared/components/chartjs.component';
import { GmapComponent } from '../../shared/components/gmap.component';
import { LoaderDirective } from '../../shared/directives/loader.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    UikitCardComponent,
    UikitTabbarComponent,
    UikitGridComponent,
    UikitAccordionComponent,
    UikitWeatherComponent,
    UikitChartsComponent,
    UikitMapsComponent,
    UikitLoaderComponent,
    UikitComponent,

  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: UikitComponent,
        children: [
          { path: 'accordion', component: UikitAccordionComponent },
          { path: 'card', component: UikitCardComponent },
          { path: 'chart', component: UikitChartsComponent },
          { path: 'grid', component: UikitGridComponent },
          { path: 'loader', component: UikitLoaderComponent },
          { path: 'maps', component: UikitMapsComponent },
          { path: 'tabbar', component: UikitTabbarComponent },
          { path: 'weather', component: UikitWeatherComponent },
          { path: '', redirectTo: 'card', pathMatch: 'full' },
        ]
      }
    ]),
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class UikitModule {

}
