import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-uikit',
  template: `
    <button routerLink="/uikit/accordion">accordion</button>
    <button routerLink="/uikit/card">cards</button>
    <button routerLink="/uikit/chart">chart</button>
    <button routerLink="/uikit/grid">grid</button>
    <button routerLink="/uikit/loader">loader</button>
    <button routerLink="/uikit/maps">maps</button>
    <button routerLink="/uikit/tabbar">tabbar</button>
    <button routerLink="/uikit/weather">weather</button>
    <hr>
    
    <router-outlet></router-outlet>
  `,
})
export class UikitComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
