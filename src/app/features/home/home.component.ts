import { Component, OnInit } from '@angular/core';
import { CounterService } from '../../shared/services/counter.service';

@Component({
  selector: 'app-home',
  template: `
    <p>
      home works!
    </p>
    
    <app-card header="ciao"></app-card>
    
    {{counterService.value}}
    <button (click)="counterService.inc()">+</button>

  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  constructor(public counterService: CounterService) { }

  ngOnInit(): void {
  }

}
