export interface Product {
  id: number;
  label: string;
  price: number;
}

export type ProductForm = Omit<Product, 'id'>;


