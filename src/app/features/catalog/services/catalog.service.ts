import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product, ProductForm } from '../model/product';
import { Observable } from 'rxjs';

@Injectable()
export class CatalogService {
  products: Product[];
  active: Product;

  constructor(private http: HttpClient) {
    this.resetHandler();
  }

  getProducts() {
    this.http.get<Product[]>('http://localhost:3000/products')
      .subscribe(res => this.products = res);
  }

  save(product: ProductForm) {
    if (this.active.id) {
      this.edit(product);
    } else {
      this.add(product);
    }
  }

  edit(product: ProductForm) {
    this.http.patch<Product>(`http://localhost:3000/products/${this.active.id}`, product)
      .subscribe(newProduct => {
        this.products = this.products.map(p => {
          return p.id === newProduct.id ? {...p, ...product} : p;
        });
      });
  }

  add(product: ProductForm) {
    this.http.post<Product>('http://localhost:3000/products', product)
      .subscribe(p => {
        this.products = [...this.products, p];
        this.resetHandler();
      });
  }

  deleteProduct(product: Product) {
    this.http.delete(`http://localhost:3000/products/${product.id}`)
      .subscribe(() => {
        this.products = this.products.filter(p => p.id !== product.id)
        if (this.active?.id === product.id) {
          this.resetHandler();
        }
      });
  }

  setActive(product: Product) {
    this.active = product;
  }

  resetHandler() {
    this.active = {} as Product;
  }

  destroy() {
    this.products = [];
  }
}
