import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../model/product';

@Component({
  selector: 'app-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <app-catalog-list-item
      *ngFor="let product of products"
      [product]="product"
      [selected]="product.id === active?.id"
      (setActive)="setActive.emit($event)"
      (delete)="delete.emit($event)"
    ></app-catalog-list-item>
  `,
  styles: [
  ]
})
export class CatalogListComponent {
  @Input() products: Product[];
  @Input() active: Product;
  @Output() setActive: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() delete: EventEmitter<Product> = new EventEmitter<Product>();

}
