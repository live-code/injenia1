import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Product, ProductForm } from '../model/product';

@Component({
  selector: 'app-catalog-form',
  template: `
    <form #f="ngForm" (submit)="saveHandler(f)">
      <input type="text" [ngModel]="active?.label" name="label" required>
      <input type="text" [ngModel]="active?.price" name="price" required>
      <hr>
      <button type="submit" [disabled]="f.invalid">
        {{active.id ? 'EDIT' : 'SAVE'}}
      </button>
      <button type="button" (click)="resetHandler()">RESET</button>
    </form>
  `,
  styles: [
  ]
})
export class CatalogFormComponent implements OnChanges {
  @ViewChild('f', { static: true}) form: NgForm;
  @Input() active: Product;
  @Output() save: EventEmitter<ProductForm> = new EventEmitter<Product>();
  @Output() clean: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.active?.currentValue.id) {
      this.form.reset();
    }
  }

  resetHandler() {
    this.form.reset();
    this.clean.emit()
  }

  saveHandler(f: NgForm) {
    this.save.emit(f.value);
    // f.reset();
  }

}
