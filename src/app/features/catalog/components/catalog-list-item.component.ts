import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../model/product';

@Component({
  selector: 'app-catalog-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      class="list-group-item"
      (click)="setActive.emit(product)"
      [ngClass]="{'active': selected}"
    >
      {{product.id}} - {{product.label}}

      <div class="pull-right">
        {{product.price}}
        <i class="fa fa-arrow-down" (click)="toggle($event)"></i>
        <i class="fa fa-trash"
           (click)="deleteProduct(product, $event)"></i>
      </div>
      

      <div *ngIf="isOpen">
        bla bla {{product.label}}
      </div>
 
    </li>
  `,
})
export class CatalogListItemComponent {
  @Input() product: Product;
  @Input() selected: boolean;
  @Output() setActive: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() delete: EventEmitter<Product> = new EventEmitter<Product>();
  isOpen = false;

  deleteProduct(product: Product, $event: MouseEvent) {
    event.stopPropagation();
    this.delete.emit(product);
  }

  toggle(event: MouseEvent) {
    event.stopPropagation();
    this.isOpen = !this.isOpen
  }

}
