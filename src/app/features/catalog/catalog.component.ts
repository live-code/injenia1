import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CatalogFormComponent } from './components/catalog-form.component';
import { Product, ProductForm } from './model/product';
import { CatalogService } from './services/catalog.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-catalog',
  template: `
  
    <app-card [header]="catalogService.active.id ? 'EDIT ' + catalogService.active?.label : 'ADD NEW PRODUCT'">
      <app-catalog-form
        [active]="catalogService.active"
        (save)="catalogService.save($event)"
        (clean)="catalogService.resetHandler()"
      ></app-catalog-form>
    </app-card>

    <app-card [header]="catalogService.products?.length + ' Products'">
      <app-catalog-list
        [products]="catalogService.products"
        [active]="catalogService.active"
        (setActive)="catalogService.setActive($event)"
        (delete)="catalogService.deleteProduct($event)"
      ></app-catalog-list>
    </app-card>
  `,
})
export class CatalogComponent implements OnDestroy {
  constructor(
    public catalogService: CatalogService,
  ) {
    this.catalogService.getProducts();
  }

  ngOnDestroy() {
    this.catalogService.destroy();
  }
}
