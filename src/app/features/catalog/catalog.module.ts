import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogComponent } from './catalog.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListItemComponent } from './components/catalog-list-item.component';
import { CatalogService } from './services/catalog.service';


const routes = [
  { path: '', component: CatalogComponent}
];

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogListComponent,
    CatalogFormComponent,
    CatalogListItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
  ],
  providers: [
    CatalogService
  ]
})
export class CatalogModule { }
