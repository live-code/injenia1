import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
      { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
      { path: 'customers', loadChildren: () => import('./features/customers/customers.module').then(m => m.CustomersModule) },
      { path: 'settings', loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule) },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
    ]),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
